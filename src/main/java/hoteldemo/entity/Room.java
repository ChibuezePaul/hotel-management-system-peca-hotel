package hoteldemo.entity;

import javax.persistence.*;

@Entity(name = "room")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String roomDescription;
    private String roomType;
    private Float roomPrice;
    private Boolean roomStatus = false;

    public Room() {
    }

    public Room(String roomDescription, String roomType, Float roomPrice) {
        this.roomDescription = roomDescription;
        this.roomType = roomType;
        this.roomPrice = roomPrice;
    }

    public Integer getId() {
        return id;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public Float getRoomPrice() {
        return roomPrice;
    }

    public void setRoomPrice(Float roomPrice) {
        this.roomPrice = roomPrice;
    }

    public Boolean getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(Boolean roomStatus) {
        this.roomStatus = roomStatus;
    }
}
