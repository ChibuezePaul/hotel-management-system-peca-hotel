package hoteldemo.controller;

import hoteldemo.entity.Room;
import hoteldemo.entity.User;
import hoteldemo.service.RoomService;
import hoteldemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.List;

@Controller
public class HotelController {

    @Autowired
    RoomService roomService;

    @Autowired
    UserService userService;

    @GetMapping({"/", "/index"})
    public String bookingPage() {
//        User user = new User();
//        user.setEmail("root@admin.com");
//
//  user.setGender("Male");
//        user.setPhoneNumber("98542369");
//        user.setUsername("root");
//        user.setPassword("root");
//        userService.createUser(user);

//        Room room = new Room();
//        room.setRoomDescription("A Room For Love Birds");
//        room.setRoomPrice((float) 1000);
//        room.setRoomType("Double Room");
//        roomService.createRoom(room);
//
//        Room room1 = new Room("A Room For The Introvert", "Single Room", (float)700);
//        roomService.createRoom(room1);
        return "index";
    }

    @GetMapping("/index0")
    public String receptionistPage() {
        return "index0";
    }

    @GetMapping("/room")
    public String roomPage(Model model) {
        model.addAttribute("rooms", roomService.findAllRoom());
        return "room";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("/dashboard")
    public String adminPage() {
        return "dashboard";
    }

    String role= "";
    @GetMapping("/signup/{role}")
    public String signupPage(Model model, @PathVariable("role") String role) {
        this.role = role;
        System.out.print(role);
        model.addAttribute("user", new User());
        return "signup";
    }

    @GetMapping("/signup")
    public String signupPag(Model model) {
        model.addAttribute("user", new User());
        return "signup";
    }

    @PostMapping("/add-user")
    public String signupPage(@ModelAttribute("user") User user ) {
        System.out.print(role);
        userService.createUser(user, role);
        return "redirect:/login";
    }

    @PostMapping("/validate")
    public String validate(WebRequest webRequest) {
        return userService.findUser(webRequest.getParameter("email"), webRequest.getParameter("password"));
    }
}
