package hoteldemo.service;

import hoteldemo.entity.Room;

import java.util.List;

public interface RoomService {
        void createRoom(Room room);
    void deleteRoom(Integer id);
    String findRoom(Boolean roomStatus);
    List<Room> findAllRoom();
}
