package hoteldemo.service;

import hoteldemo.entity.User;
import hoteldemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void createUser(User user, String role) {
        if (role.equals("")) {
            user.setRole(user.getRole());
        }
        else {
            user.setRole(role);
        }
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());
        user.setPhoneNumber(user.getPhoneNumber());
        user.setEmail(user.getEmail());
        user.setGender(user.getGender());
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public String findUser(String userEmail, String userPassword) {
        String email = null;
        String password = null;
        String role = null;
        List<User> users = userRepository.findUserByEmail(userEmail);

        if (users.isEmpty()) {
            return "failed";
        } else {
            email = users.get(0).getEmail();
            password = users.get(0).getPassword();
            role = users.get(0).getRole();

            if (userEmail.equalsIgnoreCase(email) && userPassword.equals(password) && role.equals("LV0")) {
                return "redirect:/dashboard";
            }
            else if(userEmail.equalsIgnoreCase(email) && userPassword.equals(password)  && role.equals("LV1")) {
                return "redirect:/index0";
            }
        }
        return "failed";
    }

    @Override
    public List<User> findAllUser() {
        return userRepository.findAll();
    }
}
