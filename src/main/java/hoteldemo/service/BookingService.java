package hoteldemo.service;

import hoteldemo.entity.Booking;

import java.util.List;

public interface BookingService {

    void createBooking(Booking booking);
    void deleteBooking(Integer id);
    Booking findBooking(Integer id);
    List<Booking> findAllBooking();
}
