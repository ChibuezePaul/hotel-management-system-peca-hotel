package hoteldemo.service;


import hoteldemo.entity.Room;
import hoteldemo.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService{

    @Autowired
    RoomRepository roomRepository;

    @Override
    public void createRoom(Room room) {
        roomRepository.save(room);
    }

    @Override
    public void deleteRoom(Integer id) {
        roomRepository.deleteById(id);
    }

    @Override
    public String findRoom(Boolean roomStatus) {
        return null;
    }

    @Override
    public List<Room> findAllRoom() {

        return roomRepository.findAll();
    }
}
