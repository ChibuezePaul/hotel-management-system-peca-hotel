package hoteldemo.service;

import hoteldemo.entity.User;

import java.util.List;

public interface UserService {
    void createUser(User user, String role);
    void deleteUser(Integer id);
    String findUser(String email, String password);
    List<User> findAllUser();
}
