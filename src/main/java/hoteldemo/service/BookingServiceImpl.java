package hoteldemo.service;

import hoteldemo.entity.Booking;
import hoteldemo.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Override
    public void createBooking(Booking booking) {
        bookingRepository.save(booking);
    }

    @Override
    public void deleteBooking(Integer id) {
        bookingRepository.deleteById(id);
    }

    @Override
    public Booking findBooking(Integer id) {
        return null;
    }

    @Override
    public List<Booking> findAllBooking() {
        return null;
    }
}
